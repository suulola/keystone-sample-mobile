import React, {useEffect} from 'react'
import {createChannel} from './src/services/PushNotificationController'
import {SafeAreaProvider} from 'react-native-safe-area-context'
import {
  foregroundNotificationHandler,
  requestUserPermission,
} from 'services/Notification'
import Providers from './src/navigation/index'

const App = () => {
  useEffect(() => {
    requestUserPermission()
    createChannel()
  }, [])
  useEffect(() => foregroundNotificationHandler(), [])

  return (
    <SafeAreaProvider>
      {/* @ts-ignore  */}
      <Providers />
    </SafeAreaProvider>
  )
}

export default App
