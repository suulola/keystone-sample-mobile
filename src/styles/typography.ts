import {StyleSheet} from 'react-native';
import {normalize} from 'utils/sizing';

export const font = StyleSheet.create({
  light: {fontFamily: 'SFProDisplay-Light'},
  medium: {fontFamily: 'SFProDisplay-Medium'},
  regular: {fontFamily: 'SFProDisplay-Regular'},
  bold: {fontFamily: 'SFProDisplay-Bold'},
  thin: {fontFamily: 'SFProDisplay-ThinItalic'},

});

export const color = StyleSheet.create({
  text_gray: {color: '#A0A0A0'},
  text_green: {color: '#05612E'},
  text_white: {color: '#FFFFFF'},
  text_red: {color: '#EA0015'},
  text_blue: {color: 'blue'},
});
export const font_size = StyleSheet.create({
  8: {fontSize: normalize(8)},
  9: {fontSize: normalize(9)},
  10: {fontSize: normalize(10)},
  11: {fontSize: normalize(11)},
  12: {fontSize: normalize(12)},
  13: {fontSize: normalize(13)},
  14: {fontSize: normalize(14)},
  15: {fontSize: normalize(15)},
  16: {fontSize: normalize(16)},
  17: {fontSize: normalize(17)},
  18: {fontSize: normalize(18)},
  20: {fontSize: normalize(20)},
  22: {fontSize: normalize(22)},
  24: {fontSize: normalize(24)},
  28: {fontSize: normalize(28)},
  56: {fontSize: normalize(56)},
});

export const align = StyleSheet.create({
  left: {textAlign: 'left'},
  right: {textAlign: 'right'},
  center: {textAlign: 'center'},
  vertical_top: {textAlignVertical: 'top'},
  vertical_bottom: {textAlignVertical: 'bottom'},
});
export const transform = StyleSheet.create({
  capitalize: {textTransform: 'capitalize'},
});
