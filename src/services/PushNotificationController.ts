import PushNotification, {Importance} from 'react-native-push-notification';
import {Platform} from 'react-native';
import messaging, {
  FirebaseMessagingTypes,
} from '@react-native-firebase/messaging'


PushNotification.configure({
  onRegister: function (token) {},
  onAction: function (notification: any) {
    try {
      if (notification?.action === 'open') {
        PushNotification.invokeApp(notification);
      }
    } catch (error) {
      console.log('ACTION ERROR: ', error);
    }
  },
  onNotification: function (notification) {
    try {
    } catch (error) {
    }
  },
  // TODO: Needed on actual devices
  // onRegistrationError: function (err) {
  //   console.error('ERROR: onRegistrationError', err.message, err);
  // },
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: false,
  requestPermissions: true,
});

export const createChannel = () => {
  try {
    PushNotification.createChannel(
      {
        channelId: 'task-me',
        channelName: 'TaskMe ',
        channelDescription: 'TaskMe Client Notification',
        playSound: true,
        soundName: 'default',
        importance: Importance.HIGH,
        vibrate: true,
      },
      created => console.log(`createChannel returned '${created}'`),
    );
  } catch (error) {
   console.log({error}) 
  }
};

export const launchNotification = (title: string) => {
  try {
      PushNotification.localNotification({
        channelId: 'task-me',
        title: title,
        message: 'Here we go',
        autoCancel: true,
        vibrate: true,
        groupSummary: true,
        vibration: 300,
        playSound: true,
        soundName: 'default',
        ignoreInForeground: false,
        importance: 'high',
        invokeApp: true,
        allowWhileIdle: true,
        priority: 'high',
        visibility: 'public',
      });
  } catch (error) {
    console.log({error}, 'ASK NOTIFICATION');
  }
};
