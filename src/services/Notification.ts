import messaging from '@react-native-firebase/messaging'
import { logResponse } from '../utils/helpers/logger'
import { launchNotification } from './PushNotificationController'


export const foregroundNotificationHandler = () => {
  const unsubscribe = messaging().onMessage(async remoteMessage => {
    logResponse(
      JSON.stringify({remoteMessage, from: 'app', type: 'onMessage'}),
    )
    launchNotification('On Message Notification - app in foreground')
  })

  return unsubscribe
}

async function onAppBootstrap () {
  const token = await messaging().getToken()
  logResponse(JSON.stringify({token}))
}

export const requestUserPermission = async () => {
  try {
    const authStatus = await messaging().requestPermission()
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL
  if (enabled) {
    onAppBootstrap()
  } else {
    console.log('Not authorized')
  }
  } catch (error) {
    console.log({error})
  }
}