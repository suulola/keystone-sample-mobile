import { IInventory, ISelectData } from "interface/IApp"


export const initialInventoryData: IInventory[] = [
	{
	  "id": 1,
	  "name": "Cartier ring",
		"purchasePrice": 5780,
		"type": "JEWELRY",
		"description": "Gift from my grandfather",
		"photo": "https://i.ibb.co/znXC7LQ/marcus-lewis-U63z-XX2f7ho-unsplash.jpg",
  },
  {
	  "id": 2,
	  "name": "Guitar",
		"purchasePrice": 850,
		"description": "Gift from my grandfather",
		"type": "MUSIC_INSTRUMENT",
		"photo": "https://i.ibb.co/4dfndL2/louis-hansel-M-d-J-Scwa-LE-unsplash.jpg",
  },
  {
	  "id": 3,
	  "name": "Shoe",
		"purchasePrice": 850,
		"description": "Gift from my grandfather",
		"type": "CLOTHING",
		"photo": "https://res.cloudinary.com/dxmewvier/image/upload/v1544425224/shoe.jpg",
  },
  {
	  "id": 4,
	  "name": "Native",
		"purchasePrice": 850,
		"description": "Gift from my grandfather",
		"type": "CLOTHING",
		"photo": "https://res.cloudinary.com/dxmewvier/image/upload/v1544425222/native.jpg",
  },
  {
	  "id": 5,
	  "name": "Native 2",
		"purchasePrice": 850,
		"type": "CLOTHING",
		"description": "Gift from my grandfather",
		"photo": "https://res.cloudinary.com/dxmewvier/image/upload/v1544425222/native.jpg",
  },

]

export const categoryList: ISelectData[] = [
	{name: `JEWELRY`, value: `JEWELRY`},
	{name: `CLOTHING`, value: `CLOTHING`},
	{name: `MUSIC_INSTRUMENT`, value: `MUSIC_INSTRUMENT`},
	{name: `FOOD`, value: `FOOD`},
]

export const MAX_PURCHASE_PRICE = 40000;
