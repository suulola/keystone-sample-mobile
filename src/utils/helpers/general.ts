import Clipboard from '@react-native-clipboard/clipboard'


export const wait = (timeout: number) =>
  new Promise(resolve => setTimeout(resolve, timeout));

  export const copyToClipboard = (text: string) => {
    Clipboard.setString(text)
  }