import React from 'react'


export const handleTextInput = (
  setState: React.Dispatch<React.SetStateAction<any>>,
  text: string,
  key: string,
) => {
  setState((oldState: any) => ({
    ...oldState,
    [key]: text
  }));
};

export const formatNumber = (
  number: string | number,
  length?: number | undefined,
) => {
  const stringNumber = `${number}`;
  const parsedNumber = parseFloat(stringNumber).toFixed(length ?? 0);
  var parts = parsedNumber.toString().split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return parts.join('.');
}