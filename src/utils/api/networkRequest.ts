import axios, {AxiosResponse, AxiosError} from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TOKEN} from 'utils/data/constant';

const instance = axios.create({
  baseURL: 'API',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
  },
});

instance.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    const token: string | null = await AsyncStorage.getItem(TOKEN);
    if (typeof token === 'string') {
      config.headers!.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
instance.interceptors.response.use(
  async function (response) {
    return response;
  },
  async function (error) {
    return Promise.reject(error);
  },
);

export default {
  get(url: string, request?: any) {
    return instance
      .get(url, request)
      .then((response: AxiosResponse) => Promise.resolve(response.data))
      .catch(error => Promise.reject(error?.response?.data));
  },
  post(url: string, request: any): any {
    return instance
      .post(url, request)
      .then(response => Promise.resolve(response.data))
      .catch((error: AxiosError) =>  Promise.resolve(error?.response?.data));
  },

  put(url: string, request: any) {
    return instance
      .put(url, request)
      .then(response => Promise.resolve(response.data))
      .catch((error: AxiosError) =>  Promise.resolve(error?.response?.data));
  },
  patch(url: string, request: any) {
    return instance
      .patch(url, request)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.resolve(error?.response?.data));
  },
  delete(url: string, request: any) {
    return instance
      .delete(url, {data: request})
      .then(response => Promise.resolve(response))
      .catch(error => Promise.resolve(error?.response?.data));
  },
};
