import React from 'react'
import {SafeAreaView} from 'react-native-safe-area-context'
import {background} from 'styles/background'
import {flex} from 'styles/layout'
import CustomText from 'components/CustomText'
import CustomButton from 'components/CustomButton'
import {View} from 'react-native'
import {align} from 'styles/typography'
import {Dispatch} from 'redux'
import {useDispatch} from 'react-redux'
import {USER_LOGOUT} from 'store/constant'
import {logResponse} from 'utils/helpers/logger'

const Menu = () => {
  const dispatch: Dispatch<any> = useDispatch()

  const handleLogout = () => {
    try {
      dispatch({type: USER_LOGOUT, payload: null})
    } catch (error) {
      logResponse(JSON.stringify({error}))
    }
  }
  return (
    <SafeAreaView style={[flex.flex_1, background.white, flex.justifyCenter]}>
      <CustomText
        otherStyles={[flex.flex_1, align.vertical_bottom]}
        text={`Menu Screen`}
      />
      <View style={[flex.flex_1, flex.justifyEnd]}>
        <CustomButton title={`Sign Out`} onPress={handleLogout} />
      </View>
    </SafeAreaView>
  )
}

export default Menu
