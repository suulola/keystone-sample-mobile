import {
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native'
import React, {useEffect, useState} from 'react'
import CustomText from 'components/CustomText'
import {border, flex, image} from 'styles/layout'
import {color, font} from 'styles/typography'
import {height, margin, padding, width} from 'styles/spacing'
// import Ionicons from 'react-native-vector-icons/Ionicons'
import {APP_BLUE} from 'styles/constant'
import FormInput from 'components/Input/FormInput'
import Select from 'components/Input/Select'
import {categoryList, MAX_PURCHASE_PRICE} from 'utils/data'
import {
  CameraOptions,
  ImageLibraryOptions,
  ImagePickerResponse,
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker'
import {displayToastMessage} from 'utils/display'
import {handleTextInput} from 'utils/helpers/input'
import SelectModal from 'components/Input/SelectModal'
import {
  requestCameraPermission,
  requestExternalWritePermission,
} from 'services/ImageController'
import {IInventory} from 'interface/IApp'
import {Dispatch} from 'redux'
import {useDispatch, useSelector} from 'react-redux'
import {ADD_INVENTORY} from 'store/constant'
import {RootState} from 'interface/IRedux'

interface IAddInventory {
  setLaunchModal: React.Dispatch<React.SetStateAction<boolean>>
}

const AddInventory = ({setLaunchModal}: IAddInventory) => {
  const current_total_amount: number = useSelector(
    (state: RootState) => state.app.current_total_amount,
  )

  const dispatch: Dispatch<any> = useDispatch()

  const [canSubmit, setCanSubmit] = useState(false)
  const [filePath, setFilePath] = useState('')
  const [inventoryForm, setInventoryForm] = useState({
    name: '',
    category: '',
    value: '',
    description: '',
  })
  const [showPictureModal, setShowPictureModal] = useState(false)

  useEffect(() => {
    const checkIfEmpty: Array<string> = Object.values(inventoryForm).filter(
      response => response === '',
    )
    if (filePath !== '' && checkIfEmpty.length === 0) {
      setCanSubmit(true)
    } else {
      setCanSubmit(false)
    }
  }, [inventoryForm, filePath])

  const captureImage = async () => {
    setShowPictureModal(false)
    const permission: boolean = await requestCameraPermission()
    if (!permission) {
      displayToastMessage(`Permission Issue`)
      return
    }
    let options: CameraOptions = {
      mediaType: 'photo',
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
      includeBase64: true,
    }
    launchCamera(options, response => {
      if (response.didCancel) {
        displayToastMessage('User cancelled camera picker')
        return
      } else if (response.errorCode == 'camera_unavailable') {
        displayToastMessage('Camera not available on device')
        return
      } else if (response.errorCode == 'permission') {
        displayToastMessage('Permission not satisfied')
        return
      } else if (response.errorCode == 'others') {
        displayToastMessage(response?.errorMessage ?? 'Error launching camera')
        return
      }
      if (response?.assets && response?.assets[0]?.base64) {
        setFilePath(response.assets[0].base64)
      }
    })
  }

  const chooseFile = async () => {
    setShowPictureModal(false)
    const permission: boolean = await requestExternalWritePermission()
    if (!permission) {
      displayToastMessage(`Permission Issue`)
      return
    }
    let options: ImageLibraryOptions = {
      mediaType: 'photo',
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      includeBase64: true,
    }
    launchImageLibrary(options, (response: ImagePickerResponse) => {
      if (response.didCancel) {
        displayToastMessage('User cancelled camera picker')
        return
      } else if (response.errorCode == 'camera_unavailable') {
        displayToastMessage('Camera not available on device')
        return
      } else if (response.errorCode == 'permission') {
        displayToastMessage('Permission not satisfied')
        return
      } else if (response.errorCode == 'others') {
        displayToastMessage(response?.errorMessage ?? 'Error fetching image')
        return
      }
      if (response?.assets && response?.assets[0]?.base64) {
        setFilePath(response.assets[0].base64)
      }
    })
  }

  const handleInventorySubmission = () => {
    try {
      const newInventory: IInventory = {
        id: 99,
        name: inventoryForm.name,
        type: inventoryForm.category,
        description: inventoryForm.description,
        photo: `data:image/jpeg;base64,${filePath}`,
        purchasePrice: Number(inventoryForm.value),
      }
      const totalPrice: number =
        current_total_amount + newInventory.purchasePrice
      if (totalPrice >= MAX_PURCHASE_PRICE) {
        displayToastMessage(
          `Sorry, Purchasing Price exceeds your total maximum value of €40,000`,
        )
        return
      }
      dispatch({
        type: ADD_INVENTORY,
        payload: newInventory,
      })
      setLaunchModal(false)
      displayToastMessage(`Inventory Successfully created`)
    } catch (error) {
      console.log({error})
    }
  }

  return (
    <>
      <View style={[flex.flex_1]}>
        <View
          style={[
            flex.row,
            flex.justifyBetween,
            padding.x_20,
            padding.t_20,
            padding.b_10,
          ]}
        >
          <Pressable onPress={() => setLaunchModal(false)}>
            <CustomText otherStyles={[color.text_blue]} text={`Cancel`} />
          </Pressable>
          <Pressable
            onPress={handleInventorySubmission}
            disabled={!canSubmit}
            style={[margin.r_5]}
          >
            <CustomText
              text={`Add`}
              otherStyles={[
                canSubmit ? color.text_blue : color.text_gray,
                // font.medium,
              ]}
            />
          </Pressable>
        </View>
        <ScrollView>
          <View
            style={[
              flex.flex_1,
              flex.alignCenter,
              padding.t_20,
              padding.b_20_percent,
            ]}
          >
            <Pressable
              onPress={() => setShowPictureModal(true)}
              style={[
                border.width_1,
                flex.alignCenter,
                width[180],
                height[180],
                image.aspect_1,
                border.radius_100,
                border.color_white,
                flex.justifyCenter,
                {
                  overflow: 'hidden',
                },
              ]}
            >
              {filePath === '' ? (
                <>
                  {/* <Ionicons name='camera' size={50} color={APP_BLUE} /> */}
                  <CustomText text={`Add Photo`} />
                </>
              ) : (
                <Image
                  source={{
                    uri: `data:image/jpeg;base64,${filePath}`,
                  }}
                  resizeMode='cover'
                  style={[width.full, image.aspect_1]}
                />
              )}
            </Pressable>

            <View style={[padding.x_20]}>
              <FormInput
                placeholder={`Name`}
                value={inventoryForm.name}
                onChangeText={(text: string) =>
                  handleTextInput(setInventoryForm, text, 'name')
                }
              />
              <Select
                placeholder={`Category`}
                data={categoryList}
                value={inventoryForm.category}
                onChangeText={(text: string) =>
                  handleTextInput(setInventoryForm, text, 'category')
                }
              />
              <FormInput
                keyboardType={`numeric`}
                placeholder={`Value`}
                value={inventoryForm.value}
                onChangeText={(text: string) =>
                  handleTextInput(setInventoryForm, text, 'value')
                }
              />
              <FormInput
                placeholder={`Description`}
                multiline={true}
                containerStyle={[styles.textAreaContainer]}
                inputStyle={[width.full, height.full]}
                value={inventoryForm.description}
                onChangeText={(text: string) =>
                  handleTextInput(setInventoryForm, text, 'description')
                }
              />
            </View>
          </View>
        </ScrollView>
      </View>
      <SelectModal
        data={[
          {name: `Camera`, value: `Camera`},
          {name: `File`, value: `File`},
        ]}
        title={`Select Upload Type`}
        setShowModal={setShowPictureModal}
        showModal={showPictureModal}
        value={''}
        onChangeText={(text: string) => {
          if (text === 'File') {
            chooseFile()
          } else if (text === 'Camera') {
            captureImage()
          } else {
            displayToastMessage(`Something went wrong`)
          }
        }}
      />
    </>
  )
}

export default AddInventory

const styles = StyleSheet.create({
  textAreaContainer: {
    height: 150,
    textAlignVertical: 'top',
  },
})
