import React, {useState} from 'react'
import {SafeAreaView} from 'react-native-safe-area-context'
import {background} from 'styles/background'
import {border, flex, image} from 'styles/layout'
import CustomText from 'components/CustomText'
import {FlatList, Image, Pressable, View} from 'react-native'
import {margin, padding, width} from 'styles/spacing'
// import Ionicons from 'react-native-vector-icons/Ionicons'
import {font, font_size} from 'styles/typography'
import {APP_BLUE} from 'styles/constant'
import BottomSheet from 'components/Modal/BottomSheet'
import AddInventory from './section/AddInventory'
import {useSelector} from 'react-redux'
import {RootState} from 'interface/IRedux'
import {IInventory} from 'interface/IApp'
import {formatNumber} from 'utils/helpers/input'

const Inventory = () => {
  const allInventory = useSelector((state: RootState) => state.app.inventory)

  const [launchSheet, setLaunchSheet] = useState(false)

  const COLUMN_NUM = 2

  const handleAddInventory = () => {
    setLaunchSheet(true)
  }
  return (
    <>
      <SafeAreaView style={[flex.flex_1, background.gray, padding.x_20]}>
        <FlatList
          data={allInventory}
          showsVerticalScrollIndicator={false}
          renderItem={({item}: {item: IInventory}) => (
            <View style={[width.half, margin.t_20]}>
              <View
                style={[
                  background.white,
                  border.radius_12,
                  flex.justifyCenter,
                  width.ninety,
                  padding.b_20,
                ]}
              >
                <Image
                  source={{uri: item.photo}}
                  resizeMode='cover'
                  style={[
                    border.radius_top_left_8,
                    border.radius_top_right_8,
                    image.aspect_1,
                  ]}
                />
                <CustomText
                  text={item.name}
                  left
                  otherStyles={[
                    padding.x_10,
                    // font.bold,
                    font_size[16],
                    padding.t_10,
                  ]}
                />
                <CustomText
                  text={`€${formatNumber(item.purchasePrice)}`}
                  left
                  otherStyles={[padding.x_10, font_size[16], padding.t_10]}
                />
              </View>
            </View>
          )}
          numColumns={COLUMN_NUM}
          key={COLUMN_NUM}
          ListHeaderComponent={
            <View style={[flex.row, flex.alignCenter, flex.justifyBetween]}>
              <CustomText text={`Inventory`} type={'title'} />
              <Pressable onPress={handleAddInventory}>
                {/* <Ionicons name='add-circle' size={24} color={APP_BLUE} /> */}
              </Pressable>
            </View>
          }
          ListEmptyComponent={
            <>
              <View
                style={[
                  flex.flex_1,
                  padding.x_20,
                  flex.justifyCenter,
                  flex.alignCenter,
                ]}
              >
                <CustomText text={`No data`} />
              </View>
            </>
          }
          keyExtractor={(item: any, index: number) => `time${index}`}
          contentContainerStyle={[flex.grow_1, padding.t_50, padding.b_20]}
        />
      </SafeAreaView>
      <BottomSheet setLaunchModal={setLaunchSheet} modalState={launchSheet}>
        <AddInventory setLaunchModal={setLaunchSheet} />
      </BottomSheet>
    </>
  )
}

export default Inventory
