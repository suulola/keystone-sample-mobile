import React from 'react'
import {SafeAreaView} from 'react-native-safe-area-context'
import {background} from 'styles/background'
import {flex} from 'styles/layout'
import CustomText from 'components/CustomText'

const Home = () => {
  return (
    <SafeAreaView style={[flex.flex_1, background.white, flex.justifyCenter]}>
      <CustomText text={`Home Screen`} />
    </SafeAreaView>
  )
}

export default Home
