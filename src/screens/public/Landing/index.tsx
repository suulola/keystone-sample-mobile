import {ImageBackground, StyleSheet, View} from 'react-native'
import React from 'react'
import {SafeAreaView} from 'react-native-safe-area-context'
import {background} from 'styles/background'
import {flex} from 'styles/layout'
import {landing} from 'utils/images/list'
import {height, margin, padding, width} from 'styles/spacing'
import CustomButton from 'components/CustomButton'
import {normalize} from 'utils/sizing'
import {logResponse} from 'utils/helpers/logger'
import {Dispatch} from 'redux'
import {useDispatch} from 'react-redux'
import {USER_AUTHENTICATION} from 'store/constant'
import CustomText from 'components/CustomText'

const Landing = () => {
  const dispatch: Dispatch<any> = useDispatch()

  const handleAuthenticateUser = () => {
    try {
      dispatch({type: USER_AUTHENTICATION, payload: null})
    } catch (error) {
      logResponse(JSON.stringify({error}))
    }
  }
  return (
    <SafeAreaView style={[flex.flex_1, background.white]}>
      <ImageBackground
        source={landing}
        resizeMode='contain'
        style={[width.device_width, height.device_height]}
      >
        <View style={[flex.flex_1, flex.justifyBetween, styles.innerContainer]}>
          <CustomText
            text={`Keystone Assessment Test`}
            otherStyles={[padding.t_20_percent]}
          />
          <View style={[margin.b_20]}>
            <CustomButton
              title={`Create Account`}
              onPress={handleAuthenticateUser}
            />
            <CustomButton title={`Login`} onPress={handleAuthenticateUser} />
          </View>
        </View>
      </ImageBackground>
    </SafeAreaView>
  )
}

export default Landing

const styles = StyleSheet.create({
  innerContainer: {
    paddingBottom: normalize(10),
  },
})
