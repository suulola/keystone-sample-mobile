
export interface INavigation {
  navigate: (route: any, option?: any) => void;
  push: (route: string, option?: any) => void;
  goBack: () => void;
}

export interface IComponent {
  navigation: INavigation;
  route?: {params: any};
}
