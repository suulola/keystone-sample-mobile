export interface ISelectData {
  name: string,
  value: string
}

export interface IInventory {
	id: number,
	name: string,
	type: string,
	purchasePrice: number,
	description: string,
	photo: string,
}