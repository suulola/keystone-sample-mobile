import { USER_AUTHENTICATION, USER_LOGOUT} from '../constant';

const initialUserState = {
  loggedIn: false,
};

const userReducer = (state = initialUserState, action: any) => {
  switch (action.type) {
    case USER_AUTHENTICATION:
      return {
        ...state,
        loggedIn: true,
      };
    case USER_LOGOUT:
      return {
        ...state,
        loggedIn: false,
      };
    default:
      return state;
  }
};

export default userReducer;
