import { ADD_INVENTORY } from "store/constant";
import { initialInventoryData } from "utils/data";

const initialAppData: any = {
  inventory: initialInventoryData,
  current_total_amount: initialInventoryData.reduce((sum, {purchasePrice}) => sum + purchasePrice,
  0,
)
};

const appReducer = (state = initialAppData, action: any) => {
  switch (action.type) {
    case ADD_INVENTORY:
      return {
        ...state,
        inventory: [...state.inventory,{...action.payload, id: state.inventory + 1 }],
        current_total_amount: state.current_total_amount + action.payload.purchasePrice
      };
    default:
      return state;
  }
};

export default appReducer;
