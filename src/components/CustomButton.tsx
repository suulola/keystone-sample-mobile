import React from 'react'
import {
  TouchableOpacity,
  StyleSheet,
  GestureResponderEvent,
  StyleProp,
  ViewStyle,
  TextStyle,
  ColorValue,
} from 'react-native'
import {APP_BLACK, APP_WHITE} from 'styles/constant'
import {normalize} from 'utils/sizing'
import CustomText from './CustomText'
import Loader from './Loader'
import {color} from 'styles/typography'

type LoaderSize = 'large' | 'small'

interface ICustomButton {
  title: string
  onPress: (event: GestureResponderEvent) => void
  loading?: boolean
  disabled?: boolean
  containerStyle?: StyleProp<ViewStyle>
  textStyle?: StyleProp<TextStyle>
  loaderSize?: LoaderSize
  loaderColor?: ColorValue
}

const CustomButton = ({
  title,
  onPress,
  containerStyle,
  loading,
  disabled,
  loaderSize,
  loaderColor,
}: ICustomButton) => {
  return (
    <>
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled}
        style={[
          styles.button,
          disabled === true ? styles.disabled : [styles.active],
          containerStyle,
        ]}
      >
        {loading && (
          <Loader
            color={loaderColor ? loaderColor : APP_WHITE}
            size={loaderSize ?? 'small'}
          />
        )}
        <CustomText text={title} otherStyles={[color.text_white]} />
      </TouchableOpacity>
    </>
  )
}

const styles = StyleSheet.create({
  button: {
    minHeight: normalize(50, 'height'),
    borderRadius: 8,
    width: '88%',
    borderStyle: 'solid',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
    alignSelf: 'center',
  },
  disabled: {
    backgroundColor: 'gray',
  },
  active: {
    backgroundColor: APP_BLACK,
  },
})

export default CustomButton
