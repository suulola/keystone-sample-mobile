import React from 'react'
import {Modal, Pressable, StyleProp, StyleSheet, ViewStyle} from 'react-native'
import {flex} from 'styles/layout'
import CustomText from '../CustomText'
import {background} from 'styles/background'
import {padding, width} from 'styles/spacing'
import {font} from 'styles/typography'

interface IBottomSheet {
  modalState: boolean
  setLaunchModal: (value: boolean) => void
  title?: string
  children: any
  containerStyle?: StyleProp<ViewStyle>
  innerContainerStyle?: StyleProp<ViewStyle>
}

const BottomSheet = ({
  modalState,
  setLaunchModal,
  title,
  children,
  innerContainerStyle,
  containerStyle,
}: IBottomSheet) => {
  return (
    <Modal animationType={'slide'} transparent={true} visible={modalState}>
      <Pressable
        onPress={() => setLaunchModal(false)}
        style={[flex.flex_1, flex.justifyEnd, background.modal, containerStyle]}
      >
        <Pressable
          onPress={() => {}}
          style={[
            background.gray,
            styles.innerContainer,
            width.full,
            innerContainerStyle,
          ]}
        >
          {title !== undefined && (
            <CustomText
              otherStyles={[padding.y_20, padding.l_40]}
              text={title}
              left
            />
          )}
          {children}
        </Pressable>
      </Pressable>
    </Modal>
  )
}

export default BottomSheet

const styles = StyleSheet.create({
  innerContainer: {
    minHeight: '90%',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
})
