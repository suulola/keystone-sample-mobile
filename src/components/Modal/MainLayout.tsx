import CustomText from 'components/CustomText'
import React from 'react'
import {Modal, StyleSheet, ScrollView, Pressable} from 'react-native'
import {APP_WHITE} from 'styles/constant'
import {padding, margin} from 'styles/spacing'
import {font} from 'styles/typography'

interface IMainLayout {
  modalState: boolean
  handleCloseModal: () => void
  children: any
  title?: string
}

const MainLayout = ({
  modalState,
  children,
  handleCloseModal,
  title,
}: IMainLayout) => {
  return (
    <Modal animationType={'slide'} transparent={true} visible={modalState}>
      <Pressable style={styles.outerContainer} onPress={handleCloseModal}>
        <Pressable style={styles.innerContainer} onPress={() => {}}>
          <CustomText
            text={title ?? `Select Item`}
            left
            otherStyles={[font.bold, padding.x_20, margin.b_10]}
          />
          <ScrollView showsVerticalScrollIndicator={false}>
            {children}
          </ScrollView>
        </Pressable>
      </Pressable>
    </Modal>
  )
}

export default MainLayout

const styles = StyleSheet.create({
  outerContainer: {
    backgroundColor: '#ffffffc7',
    paddingBottom: '20%',
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    backgroundColor: APP_WHITE,
    borderRadius: 10,
    paddingTop: 20,
    width: '100%',
    maxHeight: '40%',
    elevation: 10,
    maxWidth: 400,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
  },
  button: {
    marginTop: 15,
    borderColor: '#B5BBC9',
  },
})
