import {Pressable} from 'react-native'
import React, {useState} from 'react'
import FormInput from './FormInput'

import {flex} from 'styles/layout'
import {width} from 'styles/spacing'
import {ISelectData} from 'interface/IApp'
import SelectModal from './SelectModal'

interface ISelect {
  placeholder: string
  data: Array<ISelectData>
  onChangeText: (value: string) => void
  value: string
}

const Select = ({placeholder, data, onChangeText, value}: ISelect) => {
  const [showModal, setShowModal] = useState(false)

  return (
    <>
      <Pressable
        onPress={() => setShowModal(true)}
        style={[flex.row, width.full, flex.justifyCenter, flex.alignCenter]}
      >
        <FormInput
          editable={false}
          value={value}
          placeholder={placeholder}
          containerStyle={[width.full]}
          isSelect={true}
        />
      </Pressable>
      <SelectModal
        data={data}
        setShowModal={setShowModal}
        showModal={showModal}
        value={value}
        onChangeText={onChangeText}
      />
    </>
  )
}

export default Select
