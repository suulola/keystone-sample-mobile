import React from 'react'
import {
  StyleSheet,
  View,
  TextInput,
  KeyboardTypeOptions,
  StyleProp,
  ViewStyle,
  TextStyle,
  NativeSyntheticEvent,
  TextInputKeyPressEventData,
  GestureResponderEvent,
  TextInputFocusEventData,
} from 'react-native'
import {APP_MODAL, APP_TEXT_BLACK, APP_WHITE} from 'styles/constant'
import {margin, width} from 'styles/spacing'
import {font} from 'styles/typography'
import CustomText from '../CustomText'
// import Ionicons from 'react-native-vector-icons/Ionicons'
import {APP_BLACK} from 'styles/constant'
import {flex} from 'styles/layout'

type StatusTypes = 'dropdown' | 'text'
type PickerModeType = 'dropdown' | 'dialog'

declare interface IFormInput {
  pickerMode?: PickerModeType
  value?: string
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters' | undefined
  keyboardType?: KeyboardTypeOptions
  onChangeText?: (text: string) => void
  onValueChange?: (value: any, index: number) => void
  setValue?: (value: string) => void
  multiline?: boolean
  secureTextEntry?: boolean
  editable?: boolean
  onPress?: (event: GestureResponderEvent) => void
  error?: string
  maxLength?: number
  placeholder: string
  type?: StatusTypes
  options?: Array<{label: string; value: string}>
  containerStyle?: StyleProp<ViewStyle>
  inputStyle?: StyleProp<TextStyle>
  textInputStyle?: StyleProp<TextStyle>
  onKeyPress?: (e: NativeSyntheticEvent<TextInputKeyPressEventData>) => void
  touched?: boolean
  isSelect?: boolean
  onBlur?:
    | ((e: NativeSyntheticEvent<TextInputFocusEventData>) => void)
    | undefined
}

const FormInput = ({
  secureTextEntry,
  onPress,
  error,
  placeholder,
  type,
  options,
  containerStyle,
  onValueChange,
  inputStyle,
  textInputStyle,
  setValue,
  pickerMode,
  editable,
  isSelect,
  onBlur,
  touched,
  ...rest
}: IFormInput) => {
  return (
    <View style={[width.full, margin.t_10, containerStyle]}>
      <CustomText left text={placeholder} />
      <View
        style={[
          styles.inputRow,
          margin.t_5,
          inputStyle,
          isSelect && flex.alignCenter,
        ]}
      >
        <TextInput
          {...rest}
          underlineColorAndroid={'transparent'}
          returnKeyType='done'
          editable={editable ?? true}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          style={[
            styles.input,
            isSelect ? width.ninety : width.full,
            textInputStyle,
          ]}
          placeholderTextColor={APP_MODAL}
          onBlur={onBlur}
        />
        {isSelect && (
          <></>
          // <Ionicons name='caret-down-sharp' size={10} color={APP_BLACK} />
        )}
      </View>

      {error !== undefined && error.length > 0 && touched === true && (
        <CustomText left text={error} type='error' />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  input: {
    paddingLeft: 15,
    textAlignVertical: 'top',
    color: APP_TEXT_BLACK,
    width: '90%',
    // fontFamily: font.regular.fontFamily,
  },
  inputRow: {
    flexDirection: 'row',
    backgroundColor: APP_WHITE,
    width: '100%',
    height: 40,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'gray',
  },
})

export default FormInput
