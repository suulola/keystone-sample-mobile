import {Pressable} from 'react-native'
import React from 'react'
import MainLayout from 'components/Modal/MainLayout'
import CustomText from 'components/CustomText'
import Divider from 'components/Divider'
import {height, margin} from 'styles/spacing'
import {font} from 'styles/typography'
import {ISelectData} from 'interface/IApp'

interface SelectModal {
  data: Array<ISelectData>
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>
  showModal: boolean
  value: string
  onChangeText: (value: string) => void
  title?: string
}

const SelectModal = ({
  data,
  setShowModal,
  showModal,
  value,
  onChangeText,
  title,
}: SelectModal) => {
  return (
    <MainLayout
      title={title}
      modalState={showModal}
      handleCloseModal={() => setShowModal(false)}
    >
      {data.map((item, index) => (
        <Pressable
          key={index}
          onPress={() => {
            onChangeText(item.value)
            setShowModal(false)
          }}
          style={[height[50]]}
        >
          <CustomText
            text={item.name}
            // otherStyles={[item.value === value ? font.bold : font.regular]}
          />
          {index !== data.length - 1 && <Divider style={[margin.t_5]} />}
        </Pressable>
      ))}
    </MainLayout>
  )
}

export default SelectModal
