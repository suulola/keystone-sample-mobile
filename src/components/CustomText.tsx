import React from 'react'
import {Text, StyleSheet, StyleProp, TextStyle} from 'react-native'
import {APP_BLACK} from 'styles/constant'
import {font} from 'styles/typography'
import {normalize} from 'utils/sizing'

type StatusTypes = 'title' | 'subTitle' | 'error' | undefined

interface ICustomText {
  type?: StatusTypes
  text: string
  left?: boolean
  numberOfLines?: number
  otherStyles?: StyleProp<TextStyle>
}

const CustomText = ({
  type,
  text,
  numberOfLines,
  otherStyles,
  left,
}: ICustomText) => {
  return (
    <Text
      numberOfLines={numberOfLines}
      style={[
        type === 'title'
          ? [styles.title]
          : type === 'error'
          ? [styles.error]
          : [styles.subTitle],
        {textAlign: left ? 'left' : 'center'},
        otherStyles,
      ]}
    >
      {text}
    </Text>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: normalize(27),
    // fontFamily: font.bold.fontFamily,
    color: APP_BLACK,
  },

  subTitle: {
    fontSize: normalize(15),
    // fontFamily: font.regular.fontFamily,
    color: APP_BLACK,
  },
  error: {
    fontSize: normalize(13),
    lineHeight: 16,
    paddingHorizontal: 20,
    color: '#FF0118',
    marginVertical: 15,
  },
})

export default CustomText
