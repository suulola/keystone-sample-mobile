import React from 'react'
import {ActivityIndicator, ColorValue, View} from 'react-native'
import {flex} from '../styles/layout'
import {APP_BLACK} from '../styles/constant'

interface ILoader {
  size?: number | 'large' | 'small' | undefined
  color?: ColorValue
  centralize?: boolean
}

const Loader = ({size, centralize, color}: ILoader) => {
  return (
    <View
      style={[
        size === 'large' || centralize
          ? [flex.justifyCenter, flex.alignCenter]
          : {width: 30},
      ]}
    >
      <ActivityIndicator size={size ?? 'small'} color={color ?? APP_BLACK} />
    </View>
  )
}

export default Loader
