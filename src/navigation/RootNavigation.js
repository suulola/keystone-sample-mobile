import {createNavigationContainerRef} from '@react-navigation/native'

export const navigationRef = createNavigationContainerRef()

export function navigateMovement (name, params) {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params)
  } else {
    setTimeout(() => navigateMovement(name, params), 3000)
  }
}
