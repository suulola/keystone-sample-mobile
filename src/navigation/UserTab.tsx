import React from 'react'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createStackNavigator} from '@react-navigation/stack'

import * as Routes from './constant'
import TabBar from './TabBar'
import Inventory from 'screens/protected/tabs/Inventory'
import Home from 'screens/protected/tabs/Home'
import Menu from 'screens/protected/tabs/Menu'
import Search from 'screens/protected/tabs/Search'
import Detail from 'screens/protected/pages/Detail'

const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <TabBar {...props} />}
      initialRouteName={Routes.HOME_PAGE}
    >
      <Tab.Screen name={Routes.HOME_PAGE} component={Home} />
      <Tab.Screen name={Routes.INVENTORY_PAGE} component={Inventory} />
      <Tab.Screen name={Routes.SEARCH_PAGE} component={Search} />
      <Tab.Screen name={Routes.MENU_PAGE} component={Menu} />
    </Tab.Navigator>
  )
}

const UserTab = () => {
  return (
    <Stack.Navigator
      initialRouteName={Routes.TAB}
      screenOptions={{headerShown: false}}
    >
      <Stack.Screen name={Routes.TAB} component={TabNavigator} />
      <Stack.Screen name={Routes.DETAIL_PAGE} component={Detail} />
    </Stack.Navigator>
  )
}

export default UserTab
