import {NavigationContainer} from '@react-navigation/native'
import React from 'react'
import linking from './linking'
import {connect} from 'react-redux'
import UserTab from './UserTab'
import {navigationRef} from './RootNavigation'
import AuthStack from './AuthStack'
import Loader from 'components/Loader'

const Providers = ({isLoggedIn}) => {
  return (
    <NavigationContainer
      ref={navigationRef}
      linking={linking}
      fallback={<Loader centralize />}>
      {isLoggedIn ? <UserTab /> : <AuthStack />}
    </NavigationContainer>
  )
}

const mapStateToProps = state => ({
  isLoggedIn: state.user.loggedIn,
})

export default connect(mapStateToProps, null)(Providers)
