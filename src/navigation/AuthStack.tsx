import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import * as Routes from './constant'
import Landing from 'screens/public/Landing'
const Stack = createStackNavigator()

const AuthStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={Routes.LANDING_PAGE}
      screenOptions={{headerShown: false}}
    >
      <Stack.Screen name={Routes.LANDING_PAGE} component={Landing} />
    </Stack.Navigator>
  )
}

export default AuthStack
