import React from 'react'
import {View, TouchableOpacity, StyleSheet, Platform} from 'react-native'
import CustomText from '../components/CustomText'
// import Ionicons from 'react-native-vector-icons/Ionicons'
import {normalize} from 'utils/sizing'
import {APP_BLACK, APP_WHITE, APP_BLUE} from 'styles/constant'

const TabBar = ({state, descriptors, navigation}) => {
  return (
    <View style={styles.tabContainer}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key]
        let iconName
        route.name === 'Home'
          ? (iconName = 'home-outline')
          : route.name === 'Menu'
          ? (iconName = 'menu-outline')
          : route.name === 'Search'
          ? (iconName = 'search-outline')
          : (iconName = 'bar-chart-outline')

        const isFocused = state.index === index

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          })

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name)
          }
        }

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole='button'
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onPress}
            style={styles.button}
          >
            {/* <Ionicons
              name={iconName}
              size={normalize(17)}
              color={isFocused ? APP_BLUE : APP_BLACK}
            /> */}
            <CustomText
              type='subTitle'
              text={route.name}
              otherStyles={[
                styles.text,
                {color: isFocused ? APP_BLUE : APP_BLACK},
              ]}
            />
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  tabContainer: {
    flexDirection: 'row',
    backgroundColor: APP_WHITE,
    height:
      Platform.OS === 'ios' ? normalize(80, 'height') : normalize(60, 'height'),
    paddingTop: Platform.OS === 'ios' ? 10 : 0,
    justifyContent: 'space-around',
    alignItems: Platform.OS === 'ios' ? 'flex-start' : 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    paddingHorizontal: 10,
  },
  button: {
    alignItems: 'center',
  },
  text: {
    fontSize: normalize(14),
    marginVertical: 0,
    paddingHorizontal: 0,
  },
})

export default TabBar
