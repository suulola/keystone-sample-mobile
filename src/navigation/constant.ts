export const LANDING_PAGE = 'Landing'
export const INVENTORY_PAGE = 'Inventory'
export const HOME_PAGE = 'Home'
export const MENU_PAGE = 'Menu'
export const SEARCH_PAGE = 'Search'
export const DETAIL_PAGE = 'Detail'
export const TAB = 'Tab';
