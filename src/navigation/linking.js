const config = {
  screens: {
    Landing: '*',
  },
}

const linking = {
  prefixes: [
    'https://www.sample.com',
    'https://www.sample.com',
    'https://sample.com',
    'sample://',
    'sample://app',
  ],
  config,
}

export default linking
