import 'react-native-gesture-handler'
import {GestureHandlerRootView} from 'react-native-gesture-handler'
import React from 'react'
import {AppRegistry} from 'react-native'
import App from './App'
import messaging from '@react-native-firebase/messaging'
import {name as appName} from './app.json'
import {launchNotification} from './src/services/PushNotificationController'
import {logResponse} from './src/utils/helpers/logger'
import {Provider} from 'react-redux'
import configureStore from './src/store/configureStore'
import {PersistGate} from 'redux-persist/integration/react'
import Loader from 'components/Loader'
import {flex} from 'styles/layout'

messaging().setBackgroundMessageHandler(async remoteMessage => {
  logResponse(
    JSON.stringify({
      remoteMessage,
      from: 'index',
      type: 'setBackgroundMessageHandler',
    }),
  )
  launchNotification(
    'On background/quit state notification, app not in foreground',
  )
})

const {store, persistor} = configureStore()

const Sample = () => (
  <Provider store={store}>
    <PersistGate loading={<Loader />} persistor={persistor}>
      <GestureHandlerRootView style={[flex.flex_1]}>
        <App />
      </GestureHandlerRootView>
    </PersistGate>
  </Provider>
)

AppRegistry.registerComponent(appName, () => Sample)
